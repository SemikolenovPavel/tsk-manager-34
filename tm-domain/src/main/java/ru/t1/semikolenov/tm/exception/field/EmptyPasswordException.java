package ru.t1.semikolenov.tm.exception.field;

import ru.t1.semikolenov.tm.exception.AbstractException;

public final class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error! Password is empty...");
    }

}