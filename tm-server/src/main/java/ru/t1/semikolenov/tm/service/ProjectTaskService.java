package ru.t1.semikolenov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.api.repository.IProjectRepository;
import ru.t1.semikolenov.tm.api.repository.ITaskRepository;
import ru.t1.semikolenov.tm.api.service.IProjectTaskService;
import ru.t1.semikolenov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.semikolenov.tm.exception.entity.TaskNotFoundException;
import ru.t1.semikolenov.tm.exception.field.EmptyIdException;
import ru.t1.semikolenov.tm.exception.field.EmptyUserIdException;
import ru.t1.semikolenov.tm.model.Task;

import java.util.List;
import java.util.Optional;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    public ProjectTaskService(
            @NotNull final IProjectRepository projectRepository,
            @NotNull final ITaskRepository taskRepository
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(
            @NotNull String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        if (taskId.isEmpty()) throw new EmptyIdException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull final Task task = Optional.ofNullable(taskRepository.findOneById(userId, taskId))
                .orElseThrow(TaskNotFoundException::new);
        task.setProjectId(projectId);
    }

    @Override
    public void unbindTaskFromProject(
            @NotNull String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        if (taskId.isEmpty()) throw new EmptyIdException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull final Task task = Optional.ofNullable(taskRepository.findOneById(userId, taskId))
                .orElseThrow(TaskNotFoundException::new);
        task.setProjectId(null);
    }

    @Override
    public void removeProjectById(@NotNull String userId, @NotNull final String projectId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
        tasks.stream().forEach(task -> taskRepository.removeById(userId, task.getId()));
        projectRepository.removeById(userId, projectId);
    }

}