package ru.t1.semikolenov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.api.repository.IUserOwnedRepository;
import ru.t1.semikolenov.tm.enumerated.Sort;
import ru.t1.semikolenov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @NotNull
    @Override
    public M add(@NotNull final String userId, @NotNull final M model) {
        model.setUserId(userId);
        return add(model);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) {
        return records.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @NotNull final Comparator<M> comparator) {
        return records.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @NotNull final Sort sort) {
        final Comparator<M> comparator = sort.getComparator();
        return findAll(userId, comparator);
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        return records.stream()
                .filter(m -> userId.equals(m.getUserId()) && id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return findAll(userId).get(index);
    }

    @Nullable
    @Override
    public M remove(@NotNull final String userId, @NotNull final M model) {
        return removeById(userId, model.getId());
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    public long getSize(@NotNull final String userId) {
        return records.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .count();
    }

    @Override
    public boolean existsById(@NotNull final String userId, final String id) {
        return findOneById(userId, id) != null;
    }

}